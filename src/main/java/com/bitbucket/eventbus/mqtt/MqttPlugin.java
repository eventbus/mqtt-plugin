package com.bitbucket.eventbus.mqtt;

import com.bitbucket.eventbus.core.enums.Status;
import com.bitbucket.eventbus.core.interfaces.Bus;
import com.bitbucket.eventbus.core.interfaces.Pluggable;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.*;
import org.fusesource.mqtt.codec.MQTTFrame;

import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import static org.fusesource.hawtbuf.Buffer.utf8;

public class MqttPlugin implements Pluggable {
  private final String host;
  private final Integer port;
  private final String channel;
  private MQTT mqtt;
  private Status status;

  private Promise<Buffer> result;
  private Bus bus;
  private CallbackConnection connection;
  private String username = "";
  private String password = "";
  private String clientId = "";
  private boolean debug = false;
  private boolean trace = false;

  public MqttPlugin(String host, Integer port, String channel) {
    this.host = host;
    this.port = port;
    this.channel = channel;
    status = Status.OK;
  }
  public MqttPlugin build() {

    try {
      disconnect();

      mqtt = new MQTT();

    } catch (Exception e) {
      status = Status.ERROR;
    }
    return this;
  }

  private void disconnect() {
    if (connection != null) {
      connection.disconnect(null);
    }
  }


  public MqttPlugin withUsername(String username) {
    this.username = username;
    return this;
  }

  public MqttPlugin withClientId(String clientId) {
    this.clientId = clientId;
    return this;
  }

  public MqttPlugin withPassword(String password) {
    this.password = password;
    return this;
  }

  public MqttPlugin withDebug() {
    this.debug = true;
    return this;
  }

  public MqttPlugin withTrace() {
    this.trace = true;
    return this;
  }

  public Status getStatus() {
    return status;
  }


  public MqttPlugin withResult(Promise<Buffer> result) {
    this.result = result;
    return this;
  }

  public Promise<Buffer> getResult() {
    return result;
  }

  private void init(String host, Integer port) throws Exception {
    try {
      mqtt.setHost(host, port);

    } catch (URISyntaxException e) {
      System.out.println( e.getMessage() );
      status = Status.ERROR;
      System.out.println( "not initialized" );
    }
    mqtt.setClientId(clientId);
    mqtt.setUserName(username);
    mqtt.setPassword(password);

    mqtt.setCleanSession(true);
    mqtt.setKeepAlive((short) 60);

    if (trace){
      mqtt.setTracer(new Tracer(){
        @Override
        public void onReceive(MQTTFrame frame) {
          System.out.println("recv: "+frame);
        }

        @Override
        public void onSend(MQTTFrame frame) {
          System.out.println("send: "+frame);
        }

        @Override
        public void debug(String message, Object... args) {
          System.out.println(String.format("debug: "+message, args));
        }
      });
    }

    connection = mqtt.callbackConnection();

    // Start add a listener to process subscription messages, and start the
    // resume the connection so it starts receiving messages from the socket.
    connection.listener(new Listener() {
      public void onConnected() {
        System.out.println("connected");
      }

      public void onDisconnected() {
        System.out.println("disconnected");
      }

      public void onPublish(UTF8Buffer topic, Buffer payload, Runnable onComplete) {
        if (debug){
          System.out.println("MQTT CHANNEL: '" + channel + "'" );
          System.out.println("Topic: '" + topic.toString() + "'" );
          System.out.println("payload: " + payload.toString() );
        }

        if (bus != null) {
          if (debug){
            System.out.println("post to bus: ["+channel+"] [" + topic.toString().replace( "/" + channel + "/", "")+ "] data: "+ payload.utf8());
          }

          bus.post(channel, "_"+topic.toString().replace( "/" + channel + "/", ""), payload.utf8());

        }

        if (result != null) {
          result.onSuccess(payload);
        }
        onComplete.run();
      }

      public void onFailure(Throwable value) {
        System.out.println("failure: "+value);
        if (result != null) {
          result.onFailure(value);
        }
        connection.disconnect(null);
      }
    });


    connection.connect(new Callback<Void>() {
      // Once we connect..
      public void onSuccess(Void v) {

        if (debug){
          // Subscribe to a topic foo
          Topic[] topics = {new Topic(utf8("foo"), QoS.AT_LEAST_ONCE)};
          connection.subscribe(topics, new Callback<byte[]>() {
            public void onSuccess(byte[] value) {
              System.out.println("Recieved: " + new String(value, StandardCharsets.UTF_8) );
              // Once subscribed, publish a message on the same topic.
              connection.publish("foo", "Hello".getBytes(), QoS.AT_LEAST_ONCE, false, null);
            }

            public void onFailure(Throwable value) {
              if (result != null) {
                result.onFailure(value);
              }
              status = Status.ERROR;
              connection.disconnect(null);
            }
          });
        }

      }

      public void onFailure(Throwable value) {
        if (result != null) {
          result.onFailure(value);
          status = Status.ERROR;
        }
      }
    });

  }

  public MqttPlugin setBus(Bus eventBus) {
    this.bus = eventBus;
    return this;
  }

  @Override
  public void publish(Bus bus, String channel, String event, Object data) {
    if ( channel.equals(this.channel) ){

      final String topic = "/" + channel + "/" + event;

      connection.publish(
        topic,
        utf8( data.toString() ).getData(),
        QoS.AT_LEAST_ONCE,
        false,
        null);
    }
  }

  @Override
  public void subscribe(final Bus bus, final String channel, final String event, Object data) {

    if ( channel.equals(this.channel) ){

      final String topic = "/" + channel + "/" + event;

      Topic[] topics = {new Topic(utf8(topic), QoS.AT_LEAST_ONCE)};

      connection.subscribe(topics, new Callback<byte[]>() {
        public void onSuccess(byte[] value) {
          if (bus != null) {
            bus.post(channel, "_"+topic, value);
          }
        }
        public void onFailure(Throwable value) {
          if (result != null) {
            result.onFailure(value);
          }
          connection.disconnect(null);
        }
      });
    }

  }

  @Override
  public void stop() {
    bus = null;
    try {
      disconnect();
      mqtt = null;
      connection = null;
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void start() {
    try {
      init(host, port);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getChannel() {
    return channel;
  }
}
